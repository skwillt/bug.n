/*
:title:     bug.n - tiling window management
:copyright: (c) 2022 joten <https://codeberg.org/joten>
:license:  GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.txt)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

Layout__init(filename := "") {
  Global Config, Layout, Main

  Layout := {arrays: Config.layout.arrays
           , cache: {}
           , filename: filename}

  Logging_info("Initialized ``" . Layout.arrays.Count() . "`` layout array" . (Layout.arrays.Count() == 1 ? "" : "s") . ".", "Layout__init")

  For i, item in Main.tags {                ;; item: monitor
    For j, jtem in item {                   ;; jtem: view
      Layout.cache[jtem.id] := {arrays: {}, aArray: {now: "", before: ""}}
      layouts := (jtem.layouts.Length() > 0 ? jtem.layouts : Config.layout.defaults)
      For k, ktem in layouts {              ;; jtem.layouts: list of layout arrays and layout indices
        Layout.cache[jtem.id].arrays[ktem[1]] := {layouts: [], aLayout: {}}
        Layout.cache[jtem.id].arrays[ktem[1]].layouts := Layout.arrays[ktem[1]].layouts
        Layout.cache[jtem.id].arrays[ktem[1]].aLayout := {set: ktem[2], actual: 1}
      }
      Layout.cache[jtem.id].aArray.now := Layout.cache[jtem.id].aArray.before := layouts[1][1]
    }
  }
  Logging_info("Initialized ``" . Layout.cache.Count() . "`` tag" . (Layout.cache.Count() == 1 ? "" : "s") . " with their layouts.", "Layout__init")

  If (Config.restore.layout) {
    FileRead, json, % filename
    For key, value in Jxon_Load(json).cache {
      If (Layout.cache.HasKey(key)) {
        Layout.cache[key].aArray.now := (Layout.arrays.HasKey(value.aArray.now) ? value.aArray.now : Layout.cache[key].aArray.now)
        Layout.cache[key].aArray.before := (Layout.arrays.HasKey(value.aArray.before) ? value.aArray.before : Layout.cache[key].aArray.before)
        For arrayKey, arrayValue in value.arrays {
          If (Layout.arrays.HasKey(arrayKey)) {
            Layout.cache[key].arrays[arrayKey] := arrayValue
          }
        }
      }
    }
  }
}

Layout_arrange(id) {
  Global Layout, Monitor, View

  aArray := Layout.cache[id].arrays[Layout.cache[id].aArray.now]
  windows := Layout_filterWindows(View.cache[id].windows)
  If (aArray.aLayout.set == "*") {
    aArray.aLayout.actual := Max(1, Min(windows.Length(), aArray.layouts.Length()))
  }
  aLayout := aArray.layouts[aArray.aLayout.actual]
  rect := Monitor.cache[View.cache[id].monitor].workArea
  Logging_debug("Arranging " . windows.Length() . " window" . (windows.Length() == 1 ? "" : "s") . " in area {" . rect.x . ", " . rect.y . ", " . rect.w . ", " . rect.h . "} "
              . "with layout " . aLayout.label . " on view " . id . ".", "Layout_arrange")
  WD := A_WinDelay
  SetWinDelay, 0
  n := ""
  For i, item in aLayout.stacks {
    ;; Apply the relative values from the layout to the current work area position and dimensions.
    stackArea := {x: rect.x + rect.w * (RegExMatch(item.x, "p|q") ? aLayout[item.x] : item.x)
                , y: rect.y + rect.h * (RegExMatch(item.y, "p|q") ? aLayout[item.y] : item.y)
                , w: rect.w * (RegExMatch(item.w, "p|q") ? aLayout[item.w] : item.w)
                , h: rect.h * (RegExMatch(item.h, "p|q") ? aLayout[item.h] : item.h)}
    If (item.maxWin == 0) {
      ;; The stack with `maxWIn == 0` is meant to be the one for the rest of the windows; therfor it should be the last stack in the layout definition.
      n := windows.Length()
      Layout_stack(i, windows, stackArea, item.pd)
    } Else {
      maxWindows := []
      Loop, % item.maxWin {
        maxWindows.Push(windows.RemoveAt(1))
      }
      Layout_stack(i, maxWindows, stackArea, item.pd)
    }
  }
  SetWinDelay, % WD
  Tray_updateLayout(View.cache[id].monitor, StrReplace(aLayout.label, "%n%", n))
}

Layout_filterWindows(windows) {
  Global Window

  tiledWindows := []
  For i, item in windows {
    If (Window.cache[item].isTiled) {
      tiledWindows.Push(item)
    }
  }
  Return, tiledWindows
}

Layout_set(key, value, delta := False) {
  ;; `key` can be one of the following: 
  ;; "array" = with `value` as the name of a layout array, "index" = with `value` as the index of a layout inside a layout array
  ;; "pd" = the propagation direction of the active stack, "pq" = the layout variables for the width/ height factors
  Global Layout, View, Window

  id := View.aView.now
  aArray := Layout.cache[id].arrays[Layout.cache[id].aArray.now]
  aLayout := aArray.layouts[aArray.aLayout.actual]
  If (key == "array") {
    If (value == -1) {
      value := Layout.cache[id].aArray.before
    }
    Layout.cache[id].aArray.before := Layout.cache[id].aArray.now
    Layout.cache[id].aArray.now := value
  } Else If (key == "index") {
    If (delta) {
      value += aArray.aLayout.actual
    }
    If (value != "*") {
      value := Max(1, Min(value, aArray.layouts.Length()))
      aArray.aLayout.actual := value
    }
    aArray.aLayout.set := value
  } Else If (key == "pd") {
    winId := WinExist("A")
    If (Window.cache.HasKey(winId) && i := Window.cache[winId].stack) {
      If (delta) {
        value += aLayout.stacks[i].pd
      }
      value := (value > 3) ? 1 : (value < 0 ? 3 : value)
      aLayout.stacks[i].pd := value
    }
  } Else If (key == "pq") {
    If (aLayout.HasKey("p") && aLayout.HasKey("q")) {
      If (delta) {
        value := Max(0.1, Min(aLayout.p + value, 0.9))
      }
      aLayout.p := value
      aLayout.q := (1.0 - value)
    }
  }
  Layout_arrange(id)
}

Layout_shuffleStack(value, delta := False) {
  Global Layout, Window

  winId := WinExist("A")
  If (Window.cache.HasKey(winId) && Window.cache[winId].isTiled) {
    wnd := Window.cache[winId]
    aArray := Layout.cache[wnd.tag].arrays[Layout.cache[wnd.tag].aArray.now]
    aLayout := aArray.layouts[aArray.aLayout.actual]
    If (aLayout.stacks.Length() > 1) {
      index := wnd.stack
      If (delta) {
        value += index
      }
      N := aLayout.stacks.Length()
      value := (value < 1) ? N : (value > N ? 1 : value)    ;; Loop, if out of bound.
      If (value != index) {
        maxWin := aLayout.stacks[value].maxWin
        pd := aLayout.stacks[value].pd
        stack := aLayout.stacks.RemoveAt(index)
        aLayout.stacks.InsertAt(value, stack)
        ;; Transfer stack properties.
        aLayout.stacks[index].maxWin := stack.maxWin
        aLayout.stacks[value + (delta ? delta : 1)].maxWin := maxWin
        aLayout.stacks[index].pd := stack.pd
        aLayout.stacks[value + (delta ? delta : 1)].pd := pd
        Layout_arrange(wnd.tag)
      }
    }
  }
}

Layout_stack(index, windows, rect, pd) {
  Global Window

  N := windows.Length()
  w := (pd == 1) ? Round(rect.w / N) : rect.w
  h := (pd == 2) ? Round(rect.h / N) : rect.h
  delX := (pd == 1) ? w : 0
  delY := (pd == 2) ? h : 0
  For i, item in windows {
    Window_move(item, rect.x + (i - 1) * delX, rect.y + (i - 1) * delY, w, h)
    Window.cache[item].stack := index
  }
}

Layout_toggleWindow() {
  Global View, Window

  winId := WinExist("A")
  ;; Only toggle `isTiled`, if the window is tagged and can be associated with a view and therewith a layout.
  If (Window.cache.HasKey(winId) && Window.cache[winId].tag != "") {
    wnd := Window.cache[winId]
    wnd.isTiled := !wnd.isTiled
    Layout_arrange(wnd.tag)
    If (!wnd.isTiled) {
      ;; If the window is floating, set the layout symbol corresponding to the active window.
      Tray_updateLayout(View.cache[wnd.tag].monitor, "~")
    }
  }
}
