/*
:title:     bug.n - tiling window management
:copyright: (c) 2022 joten <https://codeberg.org/joten>
:license:  GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.txt)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

;; script settings
#NoEnv                        ;; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn                         ;; Enable warnings to assist with detecting common errors.
SendMode Input                ;; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%   ;; Ensures a consistent starting directory.
#SingleInstance, Force
OnExit("Main_exit")

;; pseudo main function
  Main := {NAME: "bug.n"
         , VERSION: "redux v0.0.1-beta.1"

         , appDataDir: ""
         , appIcon: A_ScriptDir . "\icon.ico"
         , configFile: A_ScriptDir . "\config.json"
         , onHideShow: False
         , procId: DllCall("GetCurrentProcessId", "UInt")
         , tags: []}
  
  Main__init()

  Logging__init(Main.appDataDir . "\_logging.md")
  Logging_insert(Main.NAME . " started.", "Main")
  Logging_writeCacheToFile(True)                          ;; overwrite

  Main_readConfigFromFile()
  
  Logging_setLevel(Config.logging.level)
  SetTimer, Logging_writeCacheToFile, % Config.timer.logging

  Monitor__init(Main.appDataDir . "\_monitor.md")
  Monitor_writeCacheToFile(True)                          ;; overwrite
  Desktop__init()

  Main_packTags()

  Layout__init(Main.appDataDir . "\_layout.json")
  View__init(Main.appDataDir . "\_view.json")
  Tray__init()

  Main_insertWindows()

  Main_writeObjectsToFiles()                              ;; overwrite
  If (Config.restore.layout || Config.restore.window) {
    SetTimer, Main_writeObjectsToFiles, % Config.timer.objects
  }

  Main_registerShellHook()
  Window_run(View.cache[View.aView.now].aWindow.now, "activate")
Return
;; end of the auto-execute section

Main__init() {
  Global Main

  EnvGet, appDataDir, APPDATA
  Main.appDataDir := appDataDir . "\" . Main.NAME
  attrib := FileExist(Main.appDataDir)
  If (attrib == "") {
    FileCreateDir, % Main.appDataDir
    If (ErrorLevel) {
      MsgBox, % "Error (" . ErrorLevel . ") creating '" . Main.appDataDir . "'. Aborting."
      ExitApp
    }
  } Else If (!InStr(attrib, "D")) {
    MsgBox, % "File path '" . Main.appDataDir . "' already exists and is not a directory. Aborting."
    ExitApp
  }
  If (FileExist(Main.appDataDir . "\config.json")) {
    Main.configFile := Main.appDataDir . "\config.json"
  }

  Menu, Tray, Tip, % Main.NAME . " " . Main.VERSION
  If (A_IsCompiled) {
    Menu, Tray, Icon, % A_ScriptFullPath, -159
  }
  ;; Set the script's tray icon and allow overwriting the icon, if using the executable.
  If (FileExist(Main.appIcon)) {
    Menu, Tray, Icon, % Main.appIcon
  }
}

Main_editConfigFile() {
  Global Main

  Run, % "open """ . Main.configFile . """"
}

Main_exit(ExitReason, ExitCode) {
  Global Window

  For key, value in Window.cache {
    If (value.isManaged) {
      Window_run(key, "show")
    }
  }
}

Main_insertWindows() {
  Global Main, View, Window

  tagged := Window__init(Main.appDataDir . "\_window.json")
  views := {}
  For i, item in tagged {
    wnd := Window.cache[item]
    views[wnd.tag] := views[wnd.tag] || wnd.isTiled
    View_insertWindow(wnd.tag, wnd.id)
  }
  For key, value in views {
    If (View.cache[key].visible) {
      If (value) {
        Layout_arrange(key)
      }
    } Else {
      View_run(key, "hide")
    }
  }
}

Main_onShellMessage(message, lParam) {
  Global Main, View, Window
  HSHELL_REDRAW :=  6
  
  If (message != HSHELL_REDRAW && !Main.onHideShow) {
    ;; `HSHELL_REDRAW` is messaged often, whithout anything having changed, exctept some window's content.
    lParam := Format("0x{:x}", lParam)
    Logging_debug("Message " . message . " received for window with id " . lParam . ".", "Main_onShellMessage")
    arrangeLayouts := {}
    If (Window.cache.HasKey(lParam) && !WinExist("ahk_id " . lParam) && !Window.cache[lParam].isHidden) {
      wnd := Window.cache[lParam]
      If (wnd.isManaged && wnd.tag != "") {
        arrangeLayouts[wnd.tag] := arrangeLayouts[wnd.tag] || (wnd.isTiled && View.cache[wnd.tag].visible)
        View_removeWindow(wnd.tag, wnd.id)
      }
      Window_remove(lParam)
      Logging_debug("Removed window with id " . lParam . ".", "Main_onShellMessage")
    } Else {
      find := Window_search()
      For i, item in find {
        wnd := Window.cache[item]
        If (wnd.isManaged && wnd.tag != "") {
          If (View.cache[wnd.tag].visible) {
            arrangeLayouts[wnd.tag] := arrangeLayouts[wnd.tag] || wnd.isTiled
          } Else {
            Window_run(wnd.id, "hide")
          }
          View_insertWindow(wnd.tag, wnd.id)
          Logging_debug("Managing window with id ``" . wnd.id . "`` on tag **" . wnd.tag . "**.", "Window_manage")
        }
      }
    }
	  For key, value in arrangeLayouts {
      Layout_arrange(key)
    }
    winId := Format("0x{:x}", WinExist("A"))
    If (Window.cache.HasKey(winId) && Window.cache[winId].tag != "") {
      tag := Window.cache[winId].tag
      If (tag == View.aView.now) {
        View.cache[tag].aWindow.before := View.cache[tag].aWindow.now
        View.cache[tag].aWindow.now := winId
        If (Window.cache[winId].isTiled) {
          Layout_arrange(tag)
        }
      } Else {
        View_activate(tag)
      }
      If (!Window.cache[winId].isTiled) {
        Tray_updateLayout(View.cache[tag].monitor, "~")
      }
    }
  }
}

Main_packTags() {
  Global Config, Main, Monitor

  For i, item in Monitor.cache {
    Main.tags.Push([])
    Main.tags[i].Push({id: i, label: " " . i . " ", layouts: []})
    For j, jtem in Config.tags[i] {
      Main.tags[i].Push({id: jtem.id
                       , label: (jtem.HasKey("label") ? jtem.label : " " . Format("{:U}", jtem.id) . " ")
                       , layouts: (jtem.HasKey("layouts") ? jtem.layouts : [])})
    }
    Logging_debug("Set ``" . Main.tags[i].Length() . "`` tag" . (Main.tags[i].Length() > 1 ? "s" : "") . " for monitor **" . i . "**.", "Main_condenseViews")
  }
  If (Config.tags.Length() > Monitor.cache.Length()) {
    n := Main.tags[Monitor.primary].Length()
    Loop, % Config.tags.Length() - Monitor.cache.Length() {
      For j, jtem in Config.tags[Monitor.cache.Length() + A_Index] {
        Main.tags[Monitor.primary].Push({id: jtem.id
                                       , label: (jtem.HasKey("label") ? jtem.label : " " . Format("{:U}", jtem.id) . " ")
                                       , layouts: (jtem.HasKey("layouts") ? jtem.layouts : [])})
      }
    }
    n := Main.tags[Monitor.primary].Length() - n
    Logging_debug("Set ``" . n . "`` additional tag" . (n > 1 ? "s" : "") . " for the primary monitor **" . Monitor.primary . "**.", "Main_condenseViews")
  }
}

Main_readConfigFromFile() {
  Global Config, Main

  Config := {filename: Main.configFile}
  
  FileRead, json, % Main.configFile
  For key, value in Jxon_Load(json) {
    If (key == "hotkeys") {
      Config.hotkeys := {}
      For k, v in value {
        Config.hotkeys[k] := v
        hotkey := StrReplace(k, "Win", "#")
        hotkey := StrReplace(hotkey, "Alt", "!")
        hotkey := StrReplace(hotkey, "Ctrl", "^")
        hotkey := StrReplace(hotkey, "Shift", "+")
        Hotkey, % hotkey, Main_run
      }
    } Else {
      Config[key] := value
    }
  }
}

Main_registerShellHook() {
  Global Tray
  
  DllCall("RegisterShellHookWindow", "UInt", WinExist("ahk_id " . Tray.cache[1].winId))
  message := DllCall("RegisterWindowMessage", "Str", "SHELLHOOK")
  OnMessage(message, "Main_onShellMessage")
}
;; SKAN: How to Hook on to Shell to receive its messages? (http://www.autohotkey.com/forum/viewtopic.php?p=123323#123323)

Main_run() {
  Global Config

  hotkey := StrReplace(A_ThisHotkey, "#", "Win")
  hotkey := StrReplace(hotkey, "!", "Alt")
  hotkey := StrReplace(hotkey, "^", "Ctrl")
  hotkey := StrReplace(hotkey, "+", "Shift")
  If (Config.hotkeys.HasKey(hotkey)) {
    func := Config.hotkeys[hotkey].function
    Logging_debug("Running function ``" . func . "`` triggered by hotkey ``" . A_ThisHotkey . "``.", "Main_run")
    If (Config.hotkeys[hotkey].HasKey("arguments")) {
      args := Config.hotkeys[hotkey].arguments
      If (func == "Target") {
        Logging_debug("Running target ``" . args[1] . "``.", "Main_run")
        Run, % args[1], % args[2]
      } Else {
        If (args.Length() == 0) {
          %func%()
        } Else If (args.Length() == 1) {
          %func%(args[1])
        } Else If (args.Length() == 2) {
          %func%(args[1], args[2])
        } Else If (args.Length() == 3) {
          %func%(args[1], args[2], args[3])
        }
      }
    } Else If (func == "ExitApp") {
      ExitApp
    } Else If (func == "Reload") {
      Reload
    }
  }
}

Main_writeObjectsToFiles(overwrite := True) {
  Global Config, Layout, View, Window

  For i, object in [Layout, View, Window] {
    If (IsObject(object) && object.filename != "") {
      text := Jxon_Dump(object, indent := 2)
      If (overwrite) {
        FileDelete, % object.filename
      }
      FileAppend, % text, % object.filename
    }
  }
}

#Include, %A_ScriptDir%\desktop.ahk
#Include, %A_ScriptDir%\jxon.ahk
#Include, %A_ScriptDir%\layout.ahk
#Include, %A_ScriptDir%\logging.ahk
#Include, %A_ScriptDir%\monitor.ahk
#Include, %A_ScriptDir%\tray.ahk
#Include, %A_ScriptDir%\view.ahk
#Include, %A_ScriptDir%\window.ahk
