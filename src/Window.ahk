/*
:title:     bug.n - tiling window management
:copyright: (c) 2022 joten <https://codeberg.org/joten>
:license:  GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.txt)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

Window__init(filename := "", interval := 4200) {
  Global Config, Window

  Window := {cache: {}
           , filename: filename
           , interval: interval
           , rules: Config.window.rules}

  If (Config.restore.window) {
    FileRead, json, % filename
    cached := Jxon_Load(json).cache
  }
  find := Window_search()
  tagged := []
  For i, item in find {
    wnd := Window.cache[item]
    If (Config.restore.window && cached.HasKey(item)) {
      wnd.isManaged := cached[item].isManaged
      wnd.isTiled := cached[item].isTiled
      wnd.tag := cached[item].tag
    }
    If (wnd.isManaged && wnd.tag != "") {
      tagged.Push(wnd.id)
    }
  }
  Return, tagged
}

Window_applyRules(winId) {
  Global View, Window
  
  For i, item in Window.rules {
    wnd := Window.cache[winId]
    If ((RegExMatch(wnd.class, item.class)            || !item.HasKey("class"))
     && (RegExMatch(wnd.hasCaption, item.hasCaption)  || !item.HasKey("hasCaption"))
     && (RegExMatch(wnd.isChild, item.isChild)        || !item.HasKey("isChild"))
     && (RegExMatch(wnd.isElevated, item.isElevated)  || !item.HasKey("isElevated"))
     && (RegExMatch(wnd.isGhost, item.isGhost)        || !item.HasKey("isGhost"))
     && (RegExMatch(wnd.isPopup, item.isPopup)        || !item.HasKey("isPopup"))
     && (RegExMatch(wnd.minMax, item.minMax)          || !item.HasKey("minMax"))
     && (RegExMatch(wnd.procPath, item.procPath)      || !item.HasKey("procPath"))
     && (RegExMatch(wnd.title, item.title)            || !item.HasKey("title"))
     && (RegExMatch(wnd.visible, item.visible)        || !item.HasKey("visible"))) {
      wnd.isManaged := item.isManaged
      wnd.isTiled := item.isTiled
      wnd.tag := (item.tag != "" ? item.tag : View.aView.now)
      ;; Following rules are not applied.
      Break
    }
  }
}

Window_close() {
  Global Window

  winId := WinExist("A")
  ;; Prevent closing windows, which are not meant to be closed, i.a. the Windows Taskbar or Desktop.
  If (Window.cache.HasKey(winId) && Window.cache[winId].isManaged) {
    Window_run(winId, "close")
  }
}

Window_getObject(winId, winClass, winProcId, winProcName, winProcPath) {
  Return, {id: winId
        , class: winClass
        , procId: winProcId
        , procName: winProcName
        , procPath: winProcPath

        , exStyle: ""
        , style: ""
        , minMax: 0
        , title: ""

        , x: 0, y: 0, w: 0, h: 0

        , hasCaption: True
        , isChild: False
        , isElevated: (!A_IsAdmin && !DllCall("OpenProcess", "UInt", 0x400, "Int", 0, "UInt", winProcId, "Ptr"))
        , isGhost: (winProcPath == "C:\Windows\System32\dwm.exe" && winClass == "Ghost")
        , isPopup: False
        , visible: False

        , isHidden: False
        , isManaged: False
        , isTiled: False
        , desktop: 0
        , stack: 0
        , tag: ""}
}
;; `isElevated` <- jeeswg: How would I mimic the windows Alt+Esc hotkey in AHK? (https://autohotkey.com/boards/viewtopic.php?p=134910&sid=192dd8fcd7839b6222826561491fcd57#p134910)

Window_getPos(winId) {
  Global Window
  Static dummy5693, rect
  DWMWA_EXTENDED_FRAME_BOUNDS := 9
  S_OK := 0x0
  
  ;;-- Get the window's dimensions
  ;; Note: Only the first 16 bytes of the RECTPlus structure are used by the DwmGetWindowAttribute and GetWindowRect functions.
  VarSetCapacity(rect, 24, 0)
  DWMRC := DllCall("dwmapi\DwmGetWindowAttribute", "Ptr", winId, "UInt", DWMWA_EXTENDED_FRAME_BOUNDS, "Ptr", &rect, "UInt", 16)
                                                 ;;-- hwnd     ; dwAttribute                        ; pvAttribute ; cbAttribute
  If (DWMRC != S_OK) {
    If ErrorLevel in -3, -4                      ;;-- Dll or function not found (older than Vista)
    {                                            ;;-- Do nothing else (for now)
    } Else {
      Logging_debug("Unknown error calling _dwmapi\DwmGetWindowAttribute_, _GetWindowRect_ used instead. "
                  . "(RC = " . DWMRC . ", ErrorLevel = " . ErrorLevel . ", A_LastError = " . A_LastError . ")", "Window_getPos")
    }
    ;;-- Collect the position and size from "GetWindowRect"
    DllCall("GetWindowRect", "Ptr", winId, "Ptr", &rect)
  }

  ;;-- Populate the output variables
  Window.cache[winId].x := rectLeft := NumGet(rect,  0, "Int")
  Window.cache[winId].y := rectTop  := NumGet(rect,  4, "Int")
  Window.cache[winId].w := w        := NumGet(rect,  8, "Int") - rectLeft   ;;-- Right minus Left
  Window.cache[winId].h := h        := NumGet(rect, 12, "Int") - rectTop    ;;-- Bottom minus Top

  Return, (DWMRC == S_OK)
}
;; jballi: [Function] WinGetPosEx v0.1 (Preview) - Get the real position and size of a window (https://autohotkey.com/boards/viewtopic.php?t=3392)

Window_getProperties(winId) {
  Global Window
  WS_CAPTION := 0x00C00000
  WS_CHILD   := 0x40000000
  WS_POPUP   := 0x80000000

  WinGetClass, visibleClass, % "ahk_id " . winId
  If (Window_isResponding(winId, "Window_getProperties")) {
    WinGet, winExStyle, ExStyle, % "ahk_id " . winId
    WinGet, winMinMax, MinMax, % "ahk_id " . winId
    WinGet, winStyle, Style, % "ahk_id " . winId
    WinGetTitle, winTitle, % "ahk_id " . winId
    Window.cache[winId].exStyle := winExStyle
    Window.cache[winId].minMax  := winMinMax
    Window.cache[winId].style   := winStyle
    Window.cache[winId].title   := winTitle
    Window.cache[winId].isChild := (winStyle & WS_CHILD ? True : False)
    Window.cache[winId].isPopup := (winStyle & WS_POPUP ? True : False)
    Window.cache[winId].visible := (visibleClass != "")
    Window.cache[winId].hasCaption := (winStyle & WS_CAPTION ? True : False)
  }
}

Window_insert(winId) {
  Global Window

  winId := Format("0x{:x}", winId)
  WinGetClass, winClass, % "ahk_id " . winId
  ;; Insist on retrieving the windows's class name; without, any other property is not accessible, too, and the window not manageable.
  If (winClass == "") {
    Logging_warning("Retrieving window class name for window with id ``" . winId . "`` (possible infinite loop).", "Window_insert")
    While (WinExist(winId)) {
      Sleep, 10
      WinGetClass, winClass, % "ahk_id " . winId
      If (winClass != "") {
        Logging_debug("Window class name retrieved for window with id ``" . winId . "``.", "Window_insert")
        Break
      }
    }
  }
  If (WinExist("ahk_id " . winId)) {
    WinGet, winProcId, PID, % "ahk_id " . winId
    WinGet, winProcName, ProcessName, % "ahk_id " . winId
    WinGet, winProcPath, ProcessPath, % "ahk_id " . winId
    Window.cache[winId] := Window_getObject(winId, winClass, winProcId, winProcName, winProcPath)
  } Else {
    Logging_debug("New window with id ``" . winId . "`` does not exist anymore.", "Window_insert")
  }
}

Window_isResponding(winId, funcName := "") {
  WM_NULL := 0x0
  sendMessageTimeout := 150   ;; in milliseconds

  If !(result := DllCall("SendMessageTimeout", "UInt", winId, "UInt", WM_NULL, "Int", 0, "Int", 0, "UInt", 0x2, "UInt", sendMessageTimeout, "UInt *", 0)) {
    Logging_warning("Window with id ``" . winId . "`` seems unresponsive.", funcName)
  }
  Return, result
}

Window_matchClientArea(winId, rect, variation := 5) {
  ;; `rect` must have keys x, y, w and h.
  ;; `test` is "edge" (rectangle similar to monitor positions and dimensions?).
  ;; `variation` may be set to a number of pixels.
  Global Window

  result := False
  If (rect.HasKey("x") && rect.HasKey("y") && rect.HasKey("w") && rect.HasKey("h")) {
    result := Abs(Window.cache[winId].x - rect.x) < variation && Abs(Window.cache[winId].y - rect.y) < variation
           && Abs(Window.cache[winId].w - rect.w) < variation && Abs(Window.cache[winId].h - rect.h) < variation
  }
  Return, result
}

Window_move(winId, x, y, w, h) {
  Global Window
  WM_ENTERSIZEMOVE := 0x00000231
  WM_EXITSIZEMOVE  := 0x00000232
  
  winId := Format("0x{:x}", winId)
/*    ;; The following message is often logged and may be temporarily disabled.
  Logging_debug("Moving window with id " . Format("{:-11}", "``" . winId . "``,") 
                 . " x: " . Format("{:4}", Format("{:i}", Window.cache[winId].x)) . " -> " . Format("{:4}", Format("{:i}", x))
                . ", y: " . Format("{:4}", Format("{:i}", Window.cache[winId].y)) . " -> " . Format("{:4}", Format("{:i}", y))
                . ", w: " . Format("{:4}", Format("{:i}", Window.cache[winId].w)) . " -> " . Format("{:4}", Format("{:i}", w))
                . ", h: " . Format("{:4}", Format("{:i}", Window.cache[winId].h)) . " -> " . Format("{:4}", Format("{:i}", h)) . ".", "Window_move")
*/
  If (Window_isResponding(winId, "Window_move") && !(Window_getPos(winId) && Window_matchClientArea(winId, {x: x, y: y, w: w, h: h}))) {
    SendMessage, WM_ENTERSIZEMOVE,,,, % "ahk_id " . winId
    WinMove, % "ahk_id " . winId,, % x, % y, % w, % h
    If (Window.cache[winId].minMax != 1 && Window_getPos(winId) && !Window_matchClientArea(winId, {x: x, y: y, w: w, h: h})) {
      x -= Window.cache[winId].x - x
      y -= Window.cache[winId].y - y
      w += w - Window.cache[winId].w - 1
      h += h - Window.cache[winId].h - 1
      WinMove, % "ahk_id " . winId,, % x, % y, % w, % h
    }
    SendMessage, WM_EXITSIZEMOVE,,,, % "ahk_id " . winId
  }
}

Window_remove(winId) {
  Global Window

  Window.cache.Delete(winId)
}

Window_run(winId, command) {
  Global Desktop, Window
  
  If (Window_isResponding(winId, "Window_" . command)) {
    Logging_debug("Running command _" . command . "_ on window with id ``" . Format("0x{:x}", winId) . "``.", "Window_run")
    If (command == "activate") {
      WinActivate, % "ahk_id " . winId
    } Else If (command == "bottom") {
      WinSet, Bottom,, % "ahk_id " . winId
    } Else If (command == "close") {
      WinClose, % "ahk_id " . winId
    } Else If (command == "hide") {
      If (Desktop.hideout > 0 && Window.cache[winid].class == "ApplicationFrameWindow") {
        Desktop_moveWindowToDesktop(winId, Desktop.hideout)
      } Else {
        WinHide, % "ahk_id " . winId
      }
      Window.cache[winid].isHidden := True
    } Else If (command == "maximize") {
      WinMaximize, % "ahk_id " . winId
    } Else If (command == "minimize") {
      WinMinimize, % "ahk_id " . winId
    } Else If (command == "restore") {
      WinRestore, % "ahk_id " . winId
    } Else If (command == "setAlwaysOnTop") {
      WinSet, AlwaysOnTop, On, % "ahk_id " . winId
    } Else If (command == "show") {
      If (Desktop.hideout > 0 && Window.cache[winid].class == "ApplicationFrameWindow") {
        Desktop_moveWindowToDesktop(winId, Desktop.aDesktop)
      } Else {
        WinShow, % "ahk_id " . winId
      }
      Window.cache[winid].isHidden := False
    } Else If (command == "toggleAlwaysOnTop") {
      WinSet, AlwaysOnTop, Toggle, % "ahk_id " . winId
    } Else If (command == "top") {
      WinSet, Top,, % "ahk_id " . winId
    } Else If (command == "unsetAlwaysOnTop") {
      WinSet, AlwaysOnTop, Off, % "ahk_id " . winId
    }
  }
}

Window_search() {
  Global Window

  find := []
  WinGet, winId, List,
  Loop, % winId {
    If (!Window.cache.HasKey(winId%A_Index%)) {
      Window_insert(winId%A_Index%)
      If (Window.cache.HasKey(winId%A_Index%)) {
        wnd := Window.cache[winId%A_Index%]
        Window_getPos(wnd.id)
        Window_getProperties(wnd.id)
        Window_applyRules(wnd.id)
        Logging_debug("New window with id " . Format("{:-11}", "``" . wnd.id . "``,") . " process ``" . wnd.procName . "`` and class name ``" . wnd.class . "`` added.", "Window_search")
        find.Push(wnd.id)
      }
    }
  }
  Return, find
}

Window_writeObjectToClipboard() {
  Global Window

  text := ""
  winId := Format("0x{:x}", WinExist("A"))
  If (!Window.cache.HasKey(winId)) {
    Window_insert(winId)
    text .= "Added window just now.`n`n"
  }
  object := Jxon_Dump(Window.cache[winId], indent := 2)
  MsgBox, 0x144, % "bug.n Window Object in Cache", % text . object . "`n`nCopy object as text to clipboard?"
  IfMsgBox, Yes
    Clipboard := object
}

Window_writeShortRuleToClipboard() {
  Global Window

  object := ""
  winId := Format("0x{:x}", WinExist("A"))
  If (Window.cache.HasKey(winId)) {
    wnd := Window.cache[winId]
    object := "{`n"
            . "  ""class"": ""^" . wnd.class . "$"",`n"
            . "  ""procPath"": ""^" . StrReplace(StrReplace(StrReplace(wnd.procPath, "\", "\\\\"), ".", "\\."), " (x86)", ".*") . "$"",`n"
            . "  ""title"": ""^" . wnd.title . "$"",`n"
            . "  ""isManaged"": " . wnd.isManaged . ", ""isTiled"": " . wnd.isTiled . ", ""tag"": """ . wnd.tag . """`n"
            . "}"
    MsgBox, 0x144, % "bug.n Window Rule", % object . "`n`nCopy object as text to clipboard?"
    IfMsgBox, Yes
      Clipboard := object
  }
}
