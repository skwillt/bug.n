/*
:title:     bug.n - tiling window management
:copyright: (c) 2022 joten <https://codeberg.org/joten>
:license:  GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.txt)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

Tray__init() {
  Global Config, Main, Tray

  Tray := {cache: []
         , height: 0}

  WinGetPos,,,, h, ahk_class Shell_TrayWnd
  Tray.height := h

  ;; Create a tray on each monitor as a child of the Windows Taskbar.
  ;; The tray contains elements for the tags/ views on each monitor and the layout symbol.
  For i, item in Main.tags {
    elements := []
    For j, jtem in item {
      elements.Push({id: "v" . jtem.id, text: jtem.label, mark: Config.tray.mark.aView
                   , backColor: Config.tray.colors.views.back, foreColor: Config.tray.colors.views.fore, fontColor: Config.tray.colors.views.font})
    }
    elements.Push({id: "lo", text: " ------ ", mark: Config.tray.mark.layout
                 , backColor: Config.tray.colors.layout.back, foreColor: Config.tray.colors.layout.fore, fontColor: Config.tray.colors.layout.font})
    Tray_insert(i, elements)
    Tray_updateViews(i, [])
  }
}

Tray_appendElement(index, id, x, text, mark, backColor, foreColor, fontColor, fontSize := "", fontName := "") {
  ;; `index` is identical to the monitor index. 'id' must be unique. `x` is the element's offset.
  ;; `text` must be a string with the maximum of characters, which the element should hold.
  ;; `mark` can contain the string `vertical` to set the according option of the progress bar.
  Local element, h, w, y

  fontSize := (fontSize != "") ? fontSize : Config.tray.font.size
  fontName := (fontName != "") ? fontName : Config.tray.font.name
  Gui, %index%: Font, % "c" . fontColor . " s" . fontSize, % fontName

  element := Tray_getElementSize(text, fontSize, fontName)
  y := 0
  w := element.w
  h := Tray.height

  Gui, %index%: Default
  Gui, Add, Text,     % "x" . x . " y" . y . " w" . w . " h" . h . " backgroundTrans vTray_" . index . "_" . id . "_event gTray_guiClick"
  Gui, Add, Progress, % "x" . x . " y" . y . " w" . w . " h" . h . " background" . backColor . " c" . foreColor . (InStr(mark, "vertical") ? " vertical" : "") . " vTray_" . index . "_" . id . "_mark"
  GuiControl,, % "Tray_" . index . "_" . id . "_mark", % Trim(StrReplace(mark, "vertical"))
  y += Round((h - element.h) / 2)
  Gui, Add, Text,     % "x" . x . " y" . y . " w" . w . " h" . h . " backgroundTrans center vTray_" . index . "_" . id . "_text", % text

  Return, w
}

Tray_getElementSize(text, fontSize := "", fontName := "") {
  Static Tray_0_test_text

  Gui, 98: Default
  Gui, Font, % " s" . fontSize, % fontName
  Gui, Add, Text, x0 y0 vTray_0_test_text, % text
  GuiControlGet, Tray_0_test_text, Pos
  Gui, Destroy

  Return, {w: Tray_0_test_textW, h: Tray_0_test_textH}
}

Tray_getObject(index, elements) {
  Global Config

  Return, {index: index
         , elements: elements
         , visible: Config.tray.visible[index <= Config.tray.visible.Length() ? index : 1]
         , winId: ""}
}

Tray_guiClick(ctrlHwnd, guiEvent, eventInfo, errLevel := "") {
  Global View
  
  GuiControlGet, ctrlName, Name, % ctrlHwnd
  Logging_debug("GUI event " . guiEvent . " registered for control " . ctrlName . ".", "Tray_guiClick")
  Window_run(View.cache[View.aView.now].aWindow.now, "activate")
}

Tray_guiContextMenu(guiHwnd, ctrlHwnd, eventInfo, isRightClick, x, y) {
  Global View

  GuiControlGet, ctrlName, Name, % ctrlHwnd
  Logging_debug("GUI event " . guiEvent . (isRightClick ? " (right click) " : "") . " registered for control " . ctrlName . ".", "Tray_guiContextMenu")
  Window_run(View.cache[View.aView.now].aWindow.now, "activate")
}

Tray_insert(index, elements) {
  ;; `index` must correlate with the monitor index.
  Global Config, Monitor, Tray

  Tray.cache[index] := Tray_getObject(index, elements)

  winTitle := "bug.n tray " . index
  winW := 0
  Gui, %index%: Default
  Gui, Destroy
  Gui, +AlwaysonTop -Caption +LabelTray_gui +LastFound +ToolWindow
  Gui, Color, % Config.tray.colors.window.back
  For i, item in elements {
    ;; The accumulated width of the previous elements set the x-offset for the next element.
    winW += Tray_appendElement(index, item.id, winW, item.text, item.mark, item.backColor, item.foreColor, item.fontColor)
  }
  x := 0
  If (Config.tray.horizontalPos == "left") {
    x := 0
  } Else If (Config.tray.horizontalPos == "right") {
    x := Monitor.cache[index].w - winW * Monitor.cache[index].scale
  } Else If (Config.tray.horizontalPos == "center") {
    x := (Monitor.cache[index].w - winW * Monitor.cache[index].scale) / 2
  } Else If (Config.tray.horizontalPos >= 0) {
    x := Config.tray.horizontalPos
  } Else If (Config.tray.horizontalPos < 0) {
    x := Monitor.cache[index].w - winW * Monitor.cache[index].scale + Config.tray.horizontalPos
  }
  x := Round(x)
  If (Tray.cache[index].visible) {
    Gui, Show, % "NoActivate      x" . x . " y0 w" . winW . " h" . Tray.height, % winTitle
  } Else {
    Gui, Show, % "NoActivate Hide x" . x . " y0 w" . winW . " h" . Tray.height, % winTitle
  }
  DHW := A_DetectHiddenWindows
  DetectHiddenWindows, On
  winId := WinExist(winTitle)
  WinSet, Transparent, % Config.tray.transparency, % "ahk_id " . winId
  DetectHiddenWindows, % DHW
  Tray.cache[index].winId := winId
  DllCall("SetParent", "UInt", winId, "UInt", Monitor.cache[index].shellTrayWnd)
	Logging_debug("Set shell tray window ``" . Monitor.cache[index].shellTrayWnd . "`` as parent to " . (Tray.cache[index].visible ? "visible" : "hidden") . " "
              . "tray window ``" . winId . "`` {x: " . x . ", y: 0, w: " . winW . ", h: " . Tray.height . "}.", "Tray_insert")
}

Tray_redraw() {
  Global Monitor, Tray

  ;; On a multi-monitor system elements disappear, if e.g. a new window is created.
  ;; bug fixed or workaround?
  For i, item in Monitor.cache {
    If (!item.isPrimary && Tray.cache[i].visible) {
      For j, jtem in Tray.cache[i].elements {
        GuiControl, MoveDraw, % "Tray_" . i . "_" . jtem.id . "_text"
      }
    }
  }
}

Tray_toggleVisibility() {
  Global Tray, View

  index := View.cache[View.aView.now].monitor
  Tray.cache[index].visible := !Tray.cache[index].visible
  If (Tray.cache[index].visible) {
    Gui, %index%: Show
  } Else {
    Gui, %index%: Cancel
  }
  Window_run(View.cache[View.aView.now].aWindow.now, "activate")
}

Tray_updateElement(index, id, text := "", mark := "", backColor := "", foreColor := "", fontColor := "") {
  Gui, %index%: Default
  If (backColor != "") {
    GuiControl, % "+Background" . backColor, % "Tray_" . index . "_" . id . "_mark"
  }
  If (foreColor != "") {
    GuiControl,          % "+c" . foreColor, % "Tray_" . index . "_" . id . "_mark"
  }
  If (fontColor != "") {
    Gui, Font, % "c" . fontColor
    GuiControl, Font, % "Tray_" . index . "_" . id . "_text"
  }
  If (mark != "") {
    GuiControl,, % "Tray_" . index . "_" . id . "_mark", % Trim(StrReplace(mark, "vertical"))
  }
  If (text != "") {
    GuiControl,, % "Tray_" . index . "_" . id . "_text", % text
  }
}

Tray_updateLayout(index, text) {
  Global Tray

  If (index <= Tray.cache.Length()) {
    Tray_updateElement(index, id := "lo", text)
  }
  Tray_redraw()
}

Tray_updateViews(index, views) {
  Global Config, Tray, View

  If (views.Length() == 0) {
    For key, value in View.cache {
      views.Push(key)
    }
  }
  If (index <= Tray.cache.Length()) {
    Gui, %index%: Default
    For i, item in views {
      If (item == View.aView.now) {
        ;; Set fore-/background colors, if the view is the active view.
        Tray_updateElement(index, id := "v" . item,,, Config.tray.colors.aView.back, Config.tray.colors.aView.fore, Config.tray.colors.aView.font)
      } Else If (View.cache[item].visible) {
        ;; Set fore-/background colors, if the view is the visible on any monitor.
        Tray_updateElement(index, id := "v" . item,,, Config.tray.colors.vView.back, Config.tray.colors.vView.fore, Config.tray.colors.vView.font)
      } Else If (View.cache[item].windows.Length() == 0) {
        ;; Set fore-/background colors, if the view is empty.
        Tray_updateElement(index, id := "v" . item,,, Config.tray.colors.eView.back, Config.tray.colors.eView.fore, Config.tray.colors.eView.font)
      } Else {
        ;; Set fore-/background colors.
        Tray_updateElement(index, id := "v" . item,,, Config.tray.colors.views.back, Config.tray.colors.views.fore, Config.tray.colors.views.font)
      }
    }
    Tray_redraw()
  }
}
