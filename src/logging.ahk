/*
:title:     bug.n - tiling window management
:copyright: (c) 2022 joten <https://codeberg.org/joten>
:license:  GNU General Public License version 3 (http://www.gnu.org/licenses/gpl-3.0.txt)

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

Logging__init(filename := "", interval := 4200, level := 5) {
  ;; If set with a timer, the logging cache is written to `filename` every `interval` milliseconds by `Logging_writeCacheToFile`.
  ;; Possible logging levels: CRITICAL = 1, ERROR    = 2, WARNING  = 3
  ;;                          INFO     = 4, DEBUG    = 5
  Global Logging

  Logging := {cache: []
            , filename: filename
            , interval: interval
            , labels: ["->", "CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"]
            , level: level
            , timeFormat: "yyyy-MM-dd HH:mm:ss"}
  
  Logging_info("Logging started on level ``" . Logging.labels[Logging.level + 1] . "``.", "Logging__init")
}

Logging_getObject(timestamp, label, src, msg) {
  entry := []
  entry[1] := timestamp
  entry[2] := label
  entry[3] := src
  entry[4] := msg
  Return, entry
}

Logging_getTimestamp() {
  Global Logging
  
  FormatTime, timestamp, , % Logging.timeFormat
  Return, timestamp
}

Logging_insert(msg, src := "", level := 0, timestamp := True) {
  ;; src normally is the function, where the logging function was called from.
  ;; If `level = 0`, the message is logged independent from the current Logging.level.
  ;; Instead of the level (integer), the label (text) is added to the entry.
  ;; If `timestamp == False`, the date and time is not added to the entry.
  Global Logging
  
  If (Logging.level >= level) {
    timestamp := (timestamp ? Logging_getTimestamp() : "")
    label := Logging.labels[level + 1]
    Logging.cache.Push(Logging_getObject(timestamp, label, src, msg))
  }
}
;; Explicit functions for the individual log levels, including timestamps.
Logging_critical(msg, src) {
  Logging_insert(msg, src, 1)
}
Logging_error(msg, src) {
  Logging_insert(msg, src, 2)
}
Logging_warning(msg, src) {
  Logging_insert(msg, src, 3)
}
Logging_info(msg, src) {
  Logging_insert(msg, src, 4)
}
Logging_debug(msg, src) {
  Logging_insert(msg, src, 5)
}

Logging_setLevel(value := 0, delta := False) {
  ;; If `delta == True`, `value` should be `-1` or `+1` to de- or increment `Logging.level`.
  ;; The minimum resulting `Logging.level` is 1, the maximum - as defined above - is 5; further de- or increments are ignored.
  Global Logging
  
  value := (delta) ? Logging.level + delta : value
  value := Min(Max(value, 1), Logging.labels.Length() - 1)
  If (value != Logging.level) {
    Logging.level := value
    Logging_insert("Level set to ``" . Logging.labels[value + 1] . "``.", "Logging_setLevel")
  }
}

Logging_writeCacheToFile(overwrite := False) {
  Global Logging

  If (Logging.filename != "") {
    text := ""
    For i, item in Logging.cache {
      ;; timestamp, level, src, msg
      text .= Format("{:19}", item[1]) . " "
            . " " . Format("{:-8}", item[2]) . " "
            . (item[3] != "" ? "**" . item[3] . "**" : "") . " "
            . item[4] . "`n"
    }
    If (overwrite) {
      FileDelete, % Logging.filename
    }
    FileAppend, % text, % Logging.filename
    Logging.cache := []
  }
}
